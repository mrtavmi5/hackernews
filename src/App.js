import "bootstrap/dist/css/bootstrap.min.css";
import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";
import CommentsPage from "./pages/CommentsPage";
import TopStoriesPage from "./pages/TopStoriesPage";
import Navbar from "./components/Navbar";
import store from "./store";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Navbar />
        <Route exact path="/" component={TopStoriesPage} />
        <Route exact path="/:storyId" component={CommentsPage} />
      </Router>
    </Provider>
  );
}

export default App;
