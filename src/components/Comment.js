import React, { Component } from "react";
import { Collapse } from "react-collapse";
import { connect } from "react-redux";
import { getComments } from "../redux/comments/actions";
import CommentAuthor from "./CommentAuthor";
import CommentContent from "./CommentContent";

class Comment extends Component {
  state = { isOpened: false };
  getKids = (arg) => {
    if (arg.isOpened) {
      this.props.getComments(this.props.comment.kids);
    }
  };
  toggleCollapse = () => {
    this.setState({ isOpened: !this.state.isOpened });
  };
  render() {
    if (!this.props.comment) {
      return <div>Loading...</div>;
    }
    const { kids } = this.props.comment;
    return (
      <div>
        <CommentAuthor author={this.props.comment.by} />
        <CommentContent content={this.props.comment.text} />
        {kids && (
          <div className="collapseButton" onClick={this.toggleCollapse}>
            [{this.state.isOpened ? "-" : "+"}]
          </div>
        )}
        {kids && (
          <Collapse isOpened={this.state.isOpened} onWork={this.getKids}>
            <ul className="commentList">
              {kids.map((commentId) => (
                <li key={commentId}>
                  <Comment
                    {...this.props}
                    comment={this.props.comments[commentId]}
                  />
                </li>
              ))}
            </ul>
          </Collapse>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  comments: state.commentsReducer.comments,
});

export default connect(mapStateToProps, { getComments })(Comment);
