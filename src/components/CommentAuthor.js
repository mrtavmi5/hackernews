import React, { Component } from "react";

class CommentAuthor extends Component {
  render() {
    return <div className="commentAuthor">{this.props.author}</div>;
  }
}

export default CommentAuthor;
