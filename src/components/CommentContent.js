import React, { Component } from "react";

class CommentContent extends Component {
  render() {
    return <div className="content">{this.props.content}</div>;
  }
}

export default CommentContent;
