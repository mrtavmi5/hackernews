import React, { Component } from "react";
import { Link } from "react-router-dom";

class FooterNote extends Component {
  render() {
    return this.props.id ? (
      <Link to={`/${this.props.id}`} className="footerNote">
        {this.props.value}
      </Link>
    ) : (
      <div className="footerNote">{this.props.value}</div>
    );
  }
}

export default FooterNote;
