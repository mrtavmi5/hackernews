import React, { Component } from "react";
import StoryHeader from "./StoryHeader";
import StoryFooter from "./StoryFooter";

class Story extends Component {
  render() {
    if (!this.props.story) {
      return <div>Loading...</div>;
    }
    return (
      <div>
        <StoryHeader story={this.props.story} />
        <StoryFooter story={this.props.story} />
      </div>
    );
  }
}

export default Story;
