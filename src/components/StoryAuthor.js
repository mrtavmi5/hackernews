import React, { Component } from "react";

class StoryAuthor extends Component {
  render() {
    return <div className="storyAuthor">@{this.props.author}</div>;
  }
}

export default StoryAuthor;
