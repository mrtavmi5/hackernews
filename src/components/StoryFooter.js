import React, { Component } from "react";
import FooterNote from "./FooterNote";

class StoryFooter extends Component {
  render() {
    const { story } = this.props;
    return (
      <div className="storyFooter">
        <FooterNote value={story.descendants + " comments"} id={story.id} />
        {" | "}
        <FooterNote value={story.score + " points"} />
      </div>
    );
  }
}

export default StoryFooter;
