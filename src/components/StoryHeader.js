import React, { Component } from "react";
import StoryTitle from "./StoryTitle";
import StoryAuthor from "./StoryAuthor";

class StoryHeader extends Component {
  render() {
    return (
      <div className="storyHeader">
        <StoryTitle url={this.props.story.url} title={this.props.story.title} />
        <StoryAuthor author={this.props.story.by} />
      </div>
    );
  }
}

export default StoryHeader;
