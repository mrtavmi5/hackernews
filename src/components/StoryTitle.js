import React, { Component } from "react";

class StoryTitle extends Component {
  render() {
    return (
      <a href={this.props.url} target="_blank" rel="noopener noreferrer">
        {this.props.title}
      </a>
    );
  }
}

export default StoryTitle;
