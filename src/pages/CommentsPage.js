import React, { Component } from "react";
import Comment from "../components/Comment";
import Story from "../components/Story";
import { connect } from "react-redux";
import { getComments } from "../redux/comments/actions";
import { getStoriesById } from "../redux/stories/actions";

class CommentsPage extends Component {
  componentDidMount() {
    const { storyId } = this.props.match.params;
    const { stories } = this.props;
    const story = stories[storyId];
    if (story && story.kids) {
      this.props.getComments(story.kids);
    }

    this.props.getStoriesById([storyId]);
  }

  componentDidUpdate(prevProps) {
    const { storyId } = this.props.match.params;
    const { stories } = this.props;
    const story = stories[storyId];
    const prevStory = prevProps.stories[storyId];
    if (!prevStory && story && story.kids) {
      this.props.getComments(story.kids);
    }
  }

  render() {
    const { comments, stories } = this.props;
    const { storyId } = this.props.match.params;
    const story = stories[storyId] || null;
    return (
      story && (
        <div className="center">
          <Story story={story} />
          {story.kids && (
            <ul className="commentList">
              {story.kids.map((commentId) => (
                <li key={commentId}>
                  <Comment comment={comments[commentId]} />
                </li>
              ))}
            </ul>
          )}
        </div>
      )
    );
  }
}

const mapStateToProps = (state) => ({
  comments: state.commentsReducer.comments,
  stories: state.storiesReducer.stories,
});

export default connect(mapStateToProps, { getComments, getStoriesById })(
  CommentsPage
);
