import React, { Component } from "react";
import Story from "../components/Story";
import { connect } from "react-redux";
import { getStories, getStoriesById } from "../redux/stories/actions";

class TopStoriesPage extends Component {
  state = { pageSize: 15 };
  componentDidMount() {
    this.props.getStories();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.storyIds.length !== this.props.storyIds.length) {
      this.props.getStoriesById(
        this.props.storyIds.slice(0, this.state.pageSize)
      );
    }
  }

  render() {
    const { stories, storyIds } = this.props;
    return (
      <div className="center">
        <ol>
          {storyIds.slice(0, this.state.pageSize).map((storyId) => (
            <li key={storyId}>
              <Story story={stories[storyId]} />
            </li>
          ))}
        </ol>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  const { storyIds, stories } = state.storiesReducer;
  return {
    storyIds,
    stories,
  };
};

export default connect(mapStateToProps, { getStories, getStoriesById })(
  TopStoriesPage
);
