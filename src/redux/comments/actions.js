import axios from "axios";
import {
  GET_COMMENTS_FAIL,
  GET_COMMENTS_START,
  GET_COMMENTS_SUCCESS,
} from "./types";
import { baseURL } from "..";

export const getComments = (commentIds) => async (dispatch) => {
  dispatch({
    type: GET_COMMENTS_START,
  });
  try {
    for (const id of commentIds) {
      const res = await axios.get(`${baseURL}/item/${id}.json`);
      dispatch({
        type: GET_COMMENTS_SUCCESS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_COMMENTS_FAIL,
      payload: error,
    });
  }
};
