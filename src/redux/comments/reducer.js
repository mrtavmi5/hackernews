import {
  GET_COMMENTS_SUCCESS,
  GET_COMMENTS_START,
  GET_COMMENTS_FAIL,
} from "./types";

const initialState = {
  comments: {},
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_COMMENTS_START:
      return {
        ...state,
      };
    case GET_COMMENTS_SUCCESS:
      const newComments = { ...state.comments };
      newComments[action.payload.id] = action.payload;
      return {
        ...state,
        comments: newComments,
      };
    case GET_COMMENTS_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
}
