import { combineReducers } from "redux";
import storiesReducer from "./stories/reducer";
import commentsReducer from "./comments/reducer";

export const baseURL = "https://hacker-news.firebaseio.com/v0";
export default combineReducers({
  storiesReducer: storiesReducer,
  commentsReducer: commentsReducer,
});
