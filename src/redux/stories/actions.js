import axios from "axios";
import {
  GET_STORY_IDS_START,
  GET_STORY_IDS_SUCCESS,
  GET_STORY_IDS_FAIL,
  GET_STORIES_BY_ID_START,
  GET_STORIES_BY_ID_FAIL,
  GET_STORIES_BY_ID_SUCCESS,
} from "./types";
import { baseURL } from "..";

export const getStoriesById = (storyIds) => async (dispatch) => {
  dispatch({
    type: GET_STORIES_BY_ID_START,
  });
  try {
    for (const id of storyIds) {
      const res = await axios.get(`${baseURL}/item/${id}.json`);
      dispatch({
        type: GET_STORIES_BY_ID_SUCCESS,
        payload: res.data,
      });
    }
  } catch (error) {
    dispatch({
      type: GET_STORIES_BY_ID_FAIL,
      payload: error,
    });
  }
};

export const getStories = () => async (dispatch) => {
  dispatch({
    type: GET_STORY_IDS_START,
  });
  try {
    const res = await axios.get(`${baseURL}/topstories.json`);
    dispatch({
      type: GET_STORY_IDS_SUCCESS,
      payload: res.data,
    });
  } catch (error) {
    dispatch({
      type: GET_STORY_IDS_FAIL,
      payload: error,
    });
  }
};
