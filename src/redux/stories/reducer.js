import {
  GET_STORY_IDS_SUCCESS,
  GET_STORY_IDS_START,
  GET_STORY_IDS_FAIL,
  GET_STORIES_BY_ID_START,
  GET_STORIES_BY_ID_SUCCESS,
  GET_STORIES_BY_ID_FAIL,
} from "./types";

const initialState = {
  storyIds: [],
  stories: {},
  loadingStoryIds: false,
  error: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_STORY_IDS_START:
      return {
        ...state,
        loadingStoryIds: true,
      };
    case GET_STORY_IDS_SUCCESS:
      return {
        ...state,
        storyIds: action.payload,
        loadingStoryIds: false,
      };
    case GET_STORY_IDS_FAIL:
      return {
        ...state,
        loadingStoryIds: false,
        error: action.payload,
      };
    case GET_STORIES_BY_ID_START:
      return {
        ...state,
      };
    case GET_STORIES_BY_ID_SUCCESS:
      const newStories = { ...state.stories };
      newStories[action.payload.id] = action.payload;
      return {
        ...state,
        stories: newStories,
      };
    case GET_STORIES_BY_ID_FAIL:
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
}
